var app = angular.module("myApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider){
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $routeProvider
    .when("/angular/", {
        title: "Angular | Home",
        template: "<h1 style='color:red;text-shadow: 1px 1px 2px #000000;'>This is home page</h1>"
    })
    .when("/angular/page1", {
        title: "Angular | Page1",
        template: "<h1 style='color:yellow;text-shadow: 1px 1px 2px #000000;'>This is page1</h1>"
    })
    .when("/angular/page2", {
        title: "Angular | Page2",
        template: "<h1 style='color:green;text-shadow: 1px 1px 2px #000000;'>This is page2</h1>"
    })
    .when("/angular/page3", {
        title: "Angular | Page3",
        template: "<h1 style='color:blue;text-shadow: 1px 1px 2px #000000;'>This is page3</h1>"
    })
    .otherwise({
        template: "Page not found."
    });

}).run(function($rootScope, $http, $location){
    $rootScope.$on("$routeChangeSuccess", function(event, current, previous){
        var title = current.title;
        if(!title) title = "Angular | 404 page";
        $rootScope.title = title;
    });
});

app.controller("menuCtrl", function($scope, $http){
    $scope.menus = [
        {name: "Home", target: "/angular/"},
        {name: "Page1", target: "/angular/page1"},
        {name: "Page2", target: "/angular/page2"},
        {name: "Page3", target: "/angular/page3"},
        {name: "404", target: "/angular/404"},
    ];
});